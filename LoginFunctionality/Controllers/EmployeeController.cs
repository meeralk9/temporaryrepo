﻿using LoginFunctionality.EmployeeData;
using LoginFunctionality.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LoginFunctionality.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeeData _empdata;
        public EmployeeController(IEmployeeeData empdata)
        {
            _empdata = empdata;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var emp = _empdata.GetEmployee();
            return View(emp); 
        }

        [HttpGet]
        public IActionResult CreateEmployee()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateEmployee(EmployeeViewModel employee)
        {
            _empdata.insert(employee);
            _empdata.Save();
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var emp = _empdata.GetEmployee(id);
            return View(emp);
        }

        [HttpPost]
        public IActionResult Delete(EmployeeViewModel employee)
        {
            _empdata.Delete(employee);
            _empdata.Save();
            return View();
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var emp = _empdata.GetEmployee(id);
            return View(emp);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var emp = _empdata.GetEmployee(id);
            return View(emp);
        }

        [HttpPost]
        public IActionResult Edit(EmployeeViewModel employee)
        {
            _empdata.Update(employee);
            _empdata.Save();
            return View();
        }
    }
}
